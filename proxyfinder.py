#!/usr/bin/python
# -*- coding: utf-8 -*-

from BeautifulSoup import BeautifulSoup
import urllib2

US_PROXY_LIST = "http://www.us-proxy.org/index.php"
UK_PROXY_LIST = 'http://free-proxy-list.net/uk-proxy.html'

class ProxyFinder():
	listUrl = US_PROXY_LIST
	proxyList = None
	maxResponseTime = 4
	strict = False
	
	def getProxyList(self):
		opener = urllib2.build_opener()
		opener.addheaders = [('User-agent', 'Mozilla/5.0')]
		page = opener.open(self.listUrl).read()
		soup = BeautifulSoup(page)
		soup.prettify()
		result = []
		rows = soup.findAll('tr')
		for row in rows:
			result.append([])
			cols = row.findAll('td')
			for col in cols:
				strings = [_string.encode('utf8') for _string in col.findAll(text=True)]
				text = ''.join(strings)
				result[-1].append(text)
		self.proxyList = result
		
	def isProxyGood(self, internalProxy):
		if len(internalProxy) < 8:
			return False
		return (internalProxy[-2] == 'yes' and ((internalProxy[-4] == 'anonymous' and not self.strict) or internalProxy[-4] == 'elite proxy') and not (internalProxy[2] == '80') )
		#       HTTPS                          Anonymous																	# Port
	
	def testProxy(self, internalProxy):
		proxy = urllib2.ProxyHandler({'http': str(internalProxy[0]+':'+internalProxy[1]), 'https': str(internalProxy[0]+':'+internalProxy[1])})
		opener = urllib2.build_opener(proxy)
		try:
			opener.open('https://www.spotify.com',timeout=self.maxResponseTime).read()
			return True
		except:
			return False
	
	def getGoodProxy(self):
		self.getProxyList()
		for proxy in self.proxyList:
			if self.isProxyGood(proxy):
				return [proxy[0],proxy[1]]
	
	def getTestedProxy(self):
		self.getProxyList()
		for proxy in self.proxyList:
			if self.isProxyGood(proxy):
				if self.testProxy(proxy):
					return [proxy[0],proxy[1]]

if __name__ == "__main__":
	print "This is a component of UMP and should not be run independently."
	print "But, oh, well, here you have a list of some tested US proxies:"
	pf = ProxyFinder()
	pf.strict = True
	pf.getProxyList()
	for proxy in pf.proxyList:
		if pf.isProxyGood(proxy):
			if pf.testProxy(proxy):
				print proxy[0],':',proxy[1]