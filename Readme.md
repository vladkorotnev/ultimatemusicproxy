# Ultimate Music Proxy

## What's it do?

* Allows you to listen to DI.fm streams in any player — just set it as the proxy in your player's settings! (Does not eliminate the ads, neither boost the quality — not implying you don't deserve more, just the way it works)
* Allows you to listen to Spotify Free not depending on the country neither having to constantly look around for proxies (beta)

## Requirements

* Python 2.7
* urllib
* httplib
* BeautifulSoup
* Internet connection

## Usage

### Command-line arguments:

`python UltimateProxy.py <port> [--enable-spotify [--united-kingdom] [--strict] [--test-proxy]] [--execute "...command..."] [2>/dev/null]`

Explanation:

* `<port>` — change the port the proxy will run on (default: 3128)
* `--enable-spotify` — automatically find and enable a proxy server
* `--united-kingdom` — use the UK proxy list instead of the US one by default
* `--strict` — allow only proxies listed as "elite proxy"
* `--test-proxy` — force to use a tested proxy
* `--execute "command"` — execute a command when the proxy is ready and serving instead of the console
* `2>/dev/null` — a way of silencing stderr in bash, in case using Spotify floods the console with trash in the logs, especially trash with the Bell symbol :-)

### Commands while running:

* Q — stop the proxy
* P — get new inner proxy from the list blindly
* T — get new inner proxy with testing proxies in the list
* ? — view current inner proxy status
* S — input inner proxy data manually
* US — set the proxy list to US
* UK — set the proxy list to UK

## Sample AppleScript to act as a Spotify shortcut

```
do shell script "ump --enable-spotify --strict --execute \"/Applications/Spotify.app/Contents/MacOS/Spotify\" 2>/dev/null"
```

Save this as an application in AppleScript editor, assign your favorite icon, place in Dock, and you're good to go :)

### Explanation:

* `do shell script` — AppleScript directive to perform a shell command
* `ump` — An alias to the `python UltimateProxy.py` created by making a small script in `/usr/bin` :)
* `--enable-spotify --strict` — sets UMP to find an elite proxy upon startup
* `--execute \"/Applications/Spotify.app/Contents/MacOS/Spotify\"` — sets UMP to start Spotify after it's ready and wait for it's termination instead of showing a console
* `2>/dev/null` — silence the bell character flood from stderr


## Known issues

* Spotify playback is stable but GUI flickers between offline and online sometimes (v. 0.9 is stable though)
* This would have worked much better as a daemon + config tool

#### by Genjitsu Gadget Lab, 2016